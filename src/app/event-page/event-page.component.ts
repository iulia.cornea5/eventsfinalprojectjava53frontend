import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Event } from 'src/app/model/event';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css'],
})
export class EventPageComponent {
  event: Event = new Event(null, null, null, null, null, null, null);

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    const eventId = this.route.snapshot.paramMap.get('id');

    this.httpClient.get('/api/events/' + eventId).subscribe(
      (response) => {
        console.log(response);
        this.event = response as Event;
        if (this.event.startDate != null)
          this.event.startDate = new Date(this.event.startDate);
        if (this.event.endDate != null)
          this.event.endDate = new Date(this.event.endDate);

        console.log(this.event);
      },
      (error) => {
        console.log(error);
        if (error.error == 'There is no event with id ' + eventId) {
          this.router.navigate(['not-found']);
        }
      }
    );
  }
}
