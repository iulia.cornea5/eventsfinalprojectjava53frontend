/*
    Copie unu la unu a clasei Event din Backend
*/
export class Event {
  id: number | null;
  name: string | null;
  description: string | null;
  location: string | null;
  startDate: Date | null;
  endDate: Date | null;
  imgUrl: string | null;

  constructor(
    id: number | null,
    name: string | null,
    description: string | null,
    location: string | null,
    startDate: Date | null,
    endDate: Date | null,
    imgUrl: string | null
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.location = location;
    this.startDate = startDate;
    this.endDate = endDate;
    this.imgUrl = imgUrl;
  }

}
